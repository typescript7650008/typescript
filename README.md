### Dominando o Typescript

## Sumário

## Sumário
1. [A História do Typescript](#a-história-do-typescript)

### A História do Typescript

Typescript é um superset (conjunto de ferramentas) da linguagem JavaScript. Pertence a Microsoft e foi criado por Anders Hejlsberg em 2012.

Usar TypeScript nos permite definir tipos em variáveis e funções, além de nos dar acesso a conceitos de programação avançados, como a Orientação a Objetos.

É um projeto de códgp aberto (Open Source) e atualmente possui 68 mil estrelas no GitHub.

### Por que o TypeScript?

JavaScript é uma linguagem fracamente tipada.
Como uso do TypeScript nós a transformamos em um linguagem fortemente tipada. Isso nos ajuda a diminuir a chance de erros no código.

Por oferecer acesso a recursos da Orientação a Objetos, nosso código fica mais robusto e escalável.

O Typescript converte nosso código para versões anteriores do ECMAScript. Assim ele irá funcionar mesmo em navegadores antigos.

`2012`: Primeiro realease
- Tipagem nos dados
- Orientação a Objetos
- Interfaces
- Compilador TypeScript (TSC)

`2013`: Preparando lançamento
- Generics
- Módulo do CommonJS (utilizados no Node)
- Enum
- Verificação "No Implicit Any"

`2014`: TypeScript 1.0
- Compilador TypeScript é relançado com melhor performance e novos recursos
- O projeto do TypeScript é movido da CodePlex para o Github
- Modificador de acesso "protected" à Orientação a Objetos
- Tipo tuple

`2015`: Adaptação para o ES6
- Suporte para o ECMAScript 2015 (ES6)
- let /const
- Template Strings
- Módulos do ES6
- Union Types
- Melhora na dedução de tipos (Type Inference)
- Decorators
- JSX
- Classes abstratas 
- Suporte ao async/await

`2016`: TypeScript 2
- Tipo Never
- Propriedades readnoly
- Object Spread

`2017`: Ajustes nos tipos
- Tipo Object
- Enums em strings
- Dynamic imports

`2018`: TypeScript 3
- Número limite de itens em tuples
- Tipos condicionais
- Tipo unknown
- Tipo array
- Tipo bigint
- Import types
- Rest parameters

`2019`: Novos recursos do ES
- Melhorias na biblioteca de DOM
- Optional Chaining
- Null Coalescing

`2020`: TypeScript 4
- Private access do ECMAScript
- ECMAScript 2020
- Melhora na chamada de promises
- Auto imports melhorados
- Template Literal Types
- Novo site do TypeScript
