## Mixin

Mixin serve para resolver um pequeno problema que o Typescript possui.

Abaixo estamos tentando fazer que a class `Sofa` extenda de duas classes `Produto` e `Movel`. Porém, isso não é possível fazer com o Typescript. 

```javascript
    class Sofa extends Produto, Movel {
        constructor(public name: string) {
            super();
       }
    }
```

Usando o conceito de mixin (na verdade podemos dizer que é uma técnica), podemos fazer da seguinte forma:

Primeiro vamos acessar o conteúdo:

https://www.typescriptlang.org/docs/handbook/mixins.html

e vamos copiar a seguinte funçao:

```javascript
    function applyMixins(derivedCtor: any, constructors: any[]) {
        constructors.forEach((baseCtor) => {
            Object.getOwnPropertyNames(baseCtor.prototype).forEach((name) => {
            Object.defineProperty(
                derivedCtor.prototype,
                name,
                Object.getOwnPropertyDescriptor(baseCtor.prototype, name) ||
                Object.create(null)
            );
            });
        });
    }
```

com isso agora vamos usá-lo na classe:

``` javascript
import applyMixins from "./apply_mixin";

class Produto {
    vender(quantidade: number) {
        return `Forma vendidos ${quantidade} deste produto`;
    }
    comprar(quantidade: number) {
        return `Forma comprados ${quantidade} deste produto`;
    }
}

class Movel {
    sentar() {
        return "Você sentou no móvel";
    }
    empurrar(metros: number) {
        return `O móvel foi empurrado por ${metros} metros.}`
    }
}

class Sofa {
    constructor(public name: string) { }
}

interface Sofa extends Produto, Movel { }

applyMixins(Sofa, [Produto, Movel]);

const produto = new Sofa("Meu Sofá");

produto.vender(2500)
```