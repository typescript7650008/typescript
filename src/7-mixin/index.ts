import applyMixins from "./apply_mixin";

class Produto {
    vender(quantidade: number) {
        return `Forma vendidos ${quantidade} deste produto`;
    }
    comprar(quantidade: number) {
        return `Forma comprados ${quantidade} deste produto`;
    }
}

class Movel {
    sentar() {
        return "Você sentou no móvel";
    }
    empurrar(metros: number) {
        return `O móvel foi empurrado por ${metros} metros.}`
    }
}

class Sofa {
    constructor(public name: string) { }
}

interface Sofa extends Produto, Movel { }

applyMixins(Sofa, [Produto, Movel]);

const produto = new Sofa("Meu Sofá");

produto.vender(2500)