## Namespaces

Namespaces é um recurso específico do TypeScript que nos permite organizar melhor os arquivos em nossos projetos.

Namespaces ou Módulos?

"A partir do ECMAScript 2015, os módulos são parte nativa da linguagem JavaScript e por isso são suportados por todos os mecanismos compátivies. Assim, para novos projetosm os módulos são o mecanismo de organização de código recomendado." - Documentação do TypeScript

É recomendado usar módulo em ves de namespaces.