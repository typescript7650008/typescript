namespace Auth {

    interface IUsuario {
        email: string;
        password: string;
    }

    interface ICadastro {
        name: string;
        email: string;
        password: string;
        birthDate: Date;
    }

    export class LoginRegister {
        login(usuario: IUsuario) {
            return usuario;
        }
        register(novoUsuario: ICadastro) {
            return novoUsuario;
        }
    }

    export class Recovery {
        recoveryPassword(): string {
            return "Enviando e-mail para recuperação de senha";
        }

        recoveryUser(): string {
            return "Enviando e-mail para recuperação de usuário";
        }
    }

}