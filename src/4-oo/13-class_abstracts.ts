interface User {
    name: string;
    email: string;
    phoneNumber: string;
}

abstract class Notificaton {

    abstract send(user: User): boolean;
}

class Email extends Notificaton {

    send(user: User): boolean {
        console.log(`Enviando email para usuário: ${user.email}`);
        return true;
    }
}

class SMS extends Notificaton {
    send(user: User): boolean {
        console.log(`Enviando sms para usuário: ${user.phoneNumber}`);
        return true;
    }
}

const email = new Email();
const sms = new SMS();

email.send({
    name: "John",
    email: "johannes@gmail.com",
    phoneNumber: "35992565898",
});

sms.send({
    name: "John",
    email: "johannes@gmail.com",
    phoneNumber: "35992565898",
});