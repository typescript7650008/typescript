interface INotificationV2 {
    send(user: UserV2): boolean;
}

interface UserV2 {
    name: string;
    email: string;
    phoneNumber: string;
}

abstract class NotificatonV2 implements NotificatonV2 {

    abstract send(user: User): boolean;
}

class EmailV2 extends Notificaton {

    send(user: User): boolean {
        console.log(`Enviando email para usuário: ${user.email}`);
        return true;
    }

}

class SMSV2 extends Notificaton {
    send(user: User): boolean {
        console.log(`Enviando sms para usuário: ${user.phoneNumber}`);
        return true;
    }

}

const email2 = new EmailV2();
const sms2 = new SMSV2();

email.send({
    name: "John",
    email: "johannes@gmail.com",
    phoneNumber: "35992565898",
});

sms.send({
    name: "John",
    email: "johannes@gmail.com",
    phoneNumber: "35992565898",
});