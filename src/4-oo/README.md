# Orientação a Objetos

## Sumário

1. [O que é a programação Orientada a Objetos](#o-que-é-a-programação-orientada-a-objetos)
2. [Herança](#herança)
3. [Encapsulamento](#encapsulamento)
4. [Polimorfismo](#polimorfismo)
5. [Abstração](#abstração)

### O que é a programação Orientada a Objetos

A Programação Orientada a Objetos é um paradigma de programação que consiste em criar as intâncias de nossos projetos levando em conta o conceito dos objetos no mundo real.

Esse estilo de programação surgiu na década de 1960 e continua sendo popular até hoje.

Quando pensamos em objetos do mundo real, percebemos que todos têm algo em comum: eles possuem características e ações.

Como construir um Objeto?

Para construir um objeto, é necessário possuir um modelo que nos ajudará a entender como o objeto será e o que ele fará.

Por exemplo: para construir uma casa, é muito comum o uso de uma planta de construção.

Quando falamos de Orientação a Objetos, esses conceitos também existem.

Ela é comum uma "planta", ou "receita", e define as caracteristicas(atributos) e ações(métodos) que nosso objeto possuirá.

Além de nos permitir criar instâncias de classes, a Orientação a Objetos funciona com quatro conceitos principais:

1. Herança
2. Encapsulamento
3. Polimorfismo
4. Abstração

### Herança 

O conteito de herança permite que uma classe herde recursos de outra classe.

A classe que irá compartilhar as informações é chamada `classe pai`.

A classe fiulha que irá herdar as informações é chamada de `classe filha`.

### Encapsulamento

Encapsulamento nos permite definir diferentes níveis de acesso para nossos atributos e métodos.

Usando os modificadores de acesso, podemos determinar até que ponto um atributo ou método é visível para outras variáveis ou classes.

Podemos dizer que assim como as cápsulas em remédios protegem o conteúdo da medicação, o encapsulamento nos ajuda a proteger a intergridade das informações de nossa classe.

Existem quatro modificadores de acesso que podemos atribuir aos nossos atributos e três aos métodos:

1. `public`: 
    - Atribuitos e métodos `públicos` podem ser acessados ou alterados por qualquer classe.
    - Podemos dizer que esse modificador de acesso é comum um `banheiro público`, onde qualquer pessoa pode utilizar.

2. `protected`: 
    - Atributos e métodos `protegidos` podem ser acessados ou alterados por meio da classe em que foram criados e por meio das classes que forem filhas dela.
    - Podemos dizer que esse modificador de acesso é como um `banheiro domiciliar`, onde os donos da casa podem usá-lo, assim como todos aqueles que eles autorizarem entrar em sua casa.
3. `private`:
    - Atributos e métodos `privados` são os mais restritos. Eles podem ser acessados ou alterados apenas por meio da classe em que foram criados.
    - Podemos dizer que esse modificador de acesso é como uma `suíte`, onde apenas quem mora no quarto pode utilizar o banheiro.
4. `readonly`: (apenas para atributos) somente de leitura e não alteração.
    - Atributo com `readonly` protegem alterações, mas permitem que sejam vistos e lidos.
    - Podemos dizer que esse modificador de acesso exclusivo para atributos é como banheiro em uma loja que vende banheiros.

### Polimorfismo

"Qualidade ou estado de ser capaz de `assumir diferentes formas`" - Oxford Languages.

Esse conceito é aplicado quando uma classe filha cria um mesmo atributo ou método de sua classe pai, mas implementando um novo comportamento ou lógica, sobrescrevendo-o.

### Abstração

Abstração é um conceito que permite criar classes abstratas, que representam atributos e métodos que podem ser utilizadas em mais de um contexto.

Essa prática nos ajuda a reaproveitar melhor o código e também a deixá-lo mais leve.

