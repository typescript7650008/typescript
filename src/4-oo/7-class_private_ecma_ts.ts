class Documento {
    private valor: string = "12345667"; // TS
    #numero: number = 35; // EcmaScript

    mostarDocumento(): number {
        return this.#numero
    }
}

class CNPJ extends Documento {
    // private valor: string = "123487541";
    #numero: number = 50;

    mostarCNPJ(): number {
        return this.#numero
    }
}

const rg = new Documento();

console.log(rg.mostarDocumento());

const cnpj = new CNPJ();
console.log(cnpj.mostarCNPJ());

//console.log("RG: " + rg.valor);
