class Usuario {
    readonly id: string = "1";
    name: string = "joao";
    private password: string = "123456";
    private readonly created_at: Date = new Date("2022-01-01");
}

const usuario = new Usuario();
console.log(usuario.id);

/**
 * READONLY -> EU CONSIGO VER A INFORMAÇÃO 
 * PRIVATE -> NÃO CONSIGO NEM VER A INFORMAÇÃO
 */