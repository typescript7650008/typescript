class Pedido {

    constructor(
        private produto: string,
        protected valorTotal: number,
        protected previsaoEntrega: Date
    ) {
    }
}

const meuPedido = new Pedido("TV 64 polegadas", 2000, new Date("2023-09-1"));

console.log(meuPedido);
