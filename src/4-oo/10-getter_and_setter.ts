class Permissao {
    constructor(
        private _name: string,
        private _nivel: number
    ) {
    }

    get name(): string {
        return this._name.toUpperCase();
    }

    set name(name: string) {
        if (name.length < 2) {
            throw new Error("Name permissions must be at least")
        }
        this._name = name;
    }
}

const permissao = new Permissao("Administrador", 1);
console.log(permissao.name);

permissao.name = "adm";