class Professor {
    name: string;
    age: number;
    matter: string;

    constructor(name: string, age: number, matter: string) {
        this.name = name;
        this.age = age;
        this.matter = matter;
    }

    introduceYourself(): string {
        return `Olá meu nome é ${this.name}, tenho ${this.age} anos, e vou lecionar ${this.matter}.`;
    }

    showNotes(...notes: number[]): number {
        let total: number = notes.reduce((total, atual) => total + atual, 0);
        return total / notes.length
    }
}

const glaucio = new Professor("Glaucio Daniel", 45, "DBA")
console.log(glaucio.introduceYourself());
const average = glaucio.showNotes(10, 5, 8, 9);
console.log(`A media das notas é: ${average}`);
