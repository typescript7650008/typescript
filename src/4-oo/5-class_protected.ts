class Domicilio {

    public cor: string;
    public endereco: object;

    constructor(cor: string, endereco: object) {
        this.cor = cor;
        this.endereco = endereco;
    }

    public tocarInterfone(): string {
        return "Interfone tocado!";
    }
}

class Casa extends Domicilio {

    protected atenderInterfone(mensagem: string): string {
        return mensagem;
    }
}

const casaDoHomer = new Casa("Rosa", { cidade: "Springfiled" });

console.log(casaDoHomer.tocarInterfone());
