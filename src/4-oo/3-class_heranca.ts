class Cadastro {
    name: string;
    birth_date: Date;

    constructor(name: string, birth_date: Date) {
        this.name = name;
        this.birth_date = birth_date;
    }
}

class Cliente extends Cadastro {
    email: string;
    company: string;

    constructor(name: string, birth_date: Date, email: string, company: string) {
        super(name, birth_date);
        this.email = email;
        this.company = company;
    }
}

const cliente = new Cliente("André", new Date("2000-01-01"), "andre@gmail.com", "XPTO");
console.log(cliente);
