interface IModel {
    id: number;
    created_at: number;
    updated_at: number;
}

interface IPeople extends IModel {
    name: string,
    age?: number,
}

interface IUser extends IPeople {
    email: string;
    password: string;
}

const joao: IUser = {
    id: 1,
    name: 'João',
    password: '1223456',
    email: "joao@gmail.com",
    age: 50,
    created_at: new Date().getTime(),
    updated_at: new Date().getTime(),
}

console.log(joao);
