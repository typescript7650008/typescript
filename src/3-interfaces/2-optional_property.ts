interface IProduto {
    name: string;
    price: number;
    description?: string;
    validate_date: Date;
}

const produtoDados: IProduto = {
    name: "Notebook",
    price: 72500,
    //description: "Notebook super potente",
    validate_date: new Date(2022, 11, 1)
}