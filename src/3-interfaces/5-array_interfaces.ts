interface ICategory {
    id: number;
    name: string;
    parent?: ICategory;
}

const frontEnd: ICategory = {
    id: 1,
    name: 'Front-End',
}

const backEnd: ICategory = {
    id: 2,
    name: 'Back-end',
}

interface Menu {
    categories: ICategory[];
}

let menu: Menu = {
    categories: [frontEnd, backEnd]
};

interface ITodo {
    [indice: number]: string;
}

let minhasTarefas: ITodo;

minhasTarefas = ["Estudar Typescript", "Estudar JavaScript", "Estudar PHP 8"];

console.log(minhasTarefas[0]);
