## Interfaces

Interface é uma estrutura que nos permite definir a forma de objetos.

Ela pode definir o tipo de propriedades, os parâmetros esperados por funções e o tipo do retorno dessas funções.

Uma interface é como um contrato, ou acordo, que fazemos com o código. Precisamos seguir as regras definidas para que o código funcione.

Em poucas palvras, uma interface cria um padrão, e nos obriga a segui-lo.

