interface ICurso {
    title: string;
    description?: string;
    readonly price: number;
    hour: number;
    classification: number;
}

const curso: ICurso = {
    title: "TypeScript",
    price: 5000,
    hour: 10,
    classification: 5
}

// curso.price = 1000 // Erro pois tentamos alterar o valor de uma propriedade readonly

console.log(curso);
