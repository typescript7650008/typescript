interface IEndereco {
    logradouro: string;
    numero: number;
    bairro: string;
    cidade: string;
}

let endereco: IEndereco;

endereco = {
    logradouro: "Rua padre petrus",
    numero: 100,
    bairro: "Boa Vista",
    cidade: "Itajubá",
}