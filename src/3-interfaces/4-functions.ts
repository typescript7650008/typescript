interface ISoma {
    (num1: number, num2: number): number;
}

let primeiraSoma: ISoma;

primeiraSoma = (n1, n2): number => {
    return n1 + n2;
}

interface ICalculos {
    somar(a: number, b: number): number;
    subtrair(a: number, b: number): number;
    multiplicar(a: number, b: number): number;
    dividir(a: number, b: number): number;
}

let calculadora: ICalculos;

function multiplicar(numero1: number, numero2: number): number {
    return numero1 * numero2;
};

const dividir = (n1: number, n2: number) => n1 / n2;

calculadora = {
    somar: (numero1: number, numero2: number) => {
        return numero1 + numero2;
    },
    subtrair: function (numero1: number, numero2: number) {
        return numero1 - numero2;
    },
    multiplicar: multiplicar,
    dividir,
}