export default class DateTime {
    constructor(dateElement = document.querySelector("#datetime > div:nth-child(2)"), hourElement = document.querySelector("#datetime time")) {
        this.dateElement = dateElement;
        this.hourElement = hourElement;
        const actualDate = new Date();
        const day = actualDate.getDate();
        const mounth = actualDate.toLocaleDateString("pt-BR", {
            month: "long"
        });
        const year = actualDate.getFullYear();
        const hour = actualDate.getHours();
        const minute = actualDate.getMinutes().toString().padStart(2, '0');
        const twoPointer = actualDate.getSeconds() % 2 === 0 ? ":" : " ";
        this.date = `${day} ${mounth} ${year}`;
        this.hour = `${hour}${twoPointer}${minute}`;
    }
    set date(content) {
        if (this.dateElement) {
            this.dateElement.innerHTML = content;
        }
    }
    set hour(content) {
        if (this.hourElement) {
            this.hourElement.innerHTML = content;
        }
    }
}
//# sourceMappingURL=DateTime.js.map