// unknown -> Tipo desconhecido

let resultado: unknown;

resultado = {
    sucesso: true,
};

resultado = "Deu tudo certo. OK!";
resultado = 200;

console.log("====================================");

/**
 * Diferença do unknown para any
 * 
 * Quando falamos do any, ele armazena em sua variável qualquer valor e essa variavel pode ser implementada em qualquer outra variavel, porém no unknown isso não é possivel
 */

const informacao: any = 150;
const informacaoDesconhecida: unknown = 250;

const primeiroNumero: number = informacao

//const segundoNumero: number = informacaoDesconhecida;