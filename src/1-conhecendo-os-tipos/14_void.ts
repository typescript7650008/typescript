// void -> tipo vazio
// Serve para especificar retorno de métodos

const meuConsole: void = console.log("Hello TypeScript!"); // Não retorna nada!

function mostrarFase(): void {
    console.log("Aprendendo TypesScript");
}

mostrarFase();