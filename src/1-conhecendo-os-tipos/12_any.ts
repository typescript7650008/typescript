// Any -> suporta qualquer tipo de dados, mas deve ser usado com cuidado.

let valor; // any

valor = "João"
valor = 2000;
valor = true;

console.log(typeof valor);

let valor2: any;
