let language = "JavaScript";

console.log(language.toLocaleUpperCase());

const userInfo = [1, "André", new Date()];

userInfo.push(2);
userInfo.push("Luiz");
userInfo.push(new Date(2021, 1, 1))

console.log(userInfo);
