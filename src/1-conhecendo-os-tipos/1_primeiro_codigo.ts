console.log("TESTE");

console.log("===========");

const firstName = "André"

console.log(`Olá ${firstName}. Seja Bem Vindo!`);

console.log("===========");

class Product {

    name: string;
    price: number;

    constructor(name: string, price: number) {
        this.name = name;
        this.price = price;
    }
}

const playstation = new Product("Playstation 5", 5000)

console.log(playstation);
