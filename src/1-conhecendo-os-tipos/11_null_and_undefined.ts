// null -> ausencia de valor, nem vazio

const element: HTMLHeadElement | null = document.querySelector("h2");
let db: string | null = "mysql, 127.0.0.1, password";
db = null;

console.log("-------------------------------");

// undefined -> variavel que ainda não teve seu valor definido.
let minhaVariavel: string | undefined;
console.log(minhaVariavel); //undefined

if (new Date().getDate() === 15) {
    minhaVariavel = "Hoje é dia 15"
}

console.log(minhaVariavel);
