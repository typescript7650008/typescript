const site: unknown = "https://hcode.com.br";
let sitesFavoritos: string[] = [];

// Forma 1
sitesFavoritos.push(site as string); // Afirma que isso é uma string

// Forma 2
sitesFavoritos.push(<string>site); // cast, afirma ao typescript que isso é uma string, trate-a como string.