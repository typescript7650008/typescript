### Dominando o Typescript

## Sumário

1. [Primeiro código TS](#primeiro-código-TS)
2. [Compilando o TypeScript para versões diferentes do EcmaScript](#compilando-o-typescript-para-versões-anteriores)
3. [Compilador TypeScript](#compilador-typescript)
4. [Conhecendo os Tipos do TypeScript](#conhecendo-os-tipos-do-typeScript)

### Primeiro código TS

Após criar um novo arquivo vamos compilar usando o comando

    yarn tsc


Também podemos adicionar a flag `--watch`, com isso o compilador ficará observando as alterações feitas.

    yarn tsc --watch

Para executar um arquivo com o node basta rodar o comando:

    node nome_arquivo.js

### Compilando o TypeScript para versões anteriores

Podemos fazer usando flags 

    tsc arquivo.js --taget "ES5"

Existe uma maneira mais facil, podemos altear através do arquivo `tsconfig.json`

### Compilador TypeScript

Os Navegadores não conseguem ler arquivo TypeScript, apenas JavaScript.

O TypeScript Compiler (TSC) é uma ferramenta que permite que transformemos o código TypeScript em JavaScript.

Podemos configurar o compilador TypeScript de três maneiras:

1 - através da linha de comando, informando as configurações através de flags.
2 - Através de um arquivo `tsconfig.json` ou jsconfig.json.
3 - Exceutando tsc --init no Terminal. Ele irá criar os arquivos de configuração automaticamente.

### Conhecendo os Tipos do TypeScript

Um tipo de dados representa o formato, ou natureza de uma informação. Por exemplo, se a informação será um número, um texto, etc.

Um dos princípios do TypeScript é a definição dos tipos que as variáveis irão possuir, nos ajudando a ter um maior controle sobre essas informações.

#### Dedução de Tipos (Type Inference)

O JavaScript é uma linguagem de tipagem dinâmica.

Isso Significa que, mesmo que não informamos um tipo de dados para uma variável, o JavaScript irá deduzir o tipo dela e armazená-lo de maneira interna.

O TypeScript herda esse conceito do JS.

