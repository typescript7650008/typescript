### Funções

Funcão é um bloco de código que podemos armazenar e executar em mais de um local em nosso código.

Uma função é composta de três partes:

- Nome da função
- Lista de prâmetros
- Código que a função irá executar

Com TypScript podemos definir nas funções a tipagem:

- Dos parâmetros da função.
- Do retorno da função.

Além disso, podemos ter acesso a recursos avançados de funções, como:

- Arrow functions
- Rest parameters
