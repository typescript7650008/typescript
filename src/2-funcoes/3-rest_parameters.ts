function somaRenda(...meses: number[]): number {
    return meses.reduce((total: number, atual: number) => total + atual, 0);
}

console.log(somaRenda(1000, 2000, 3000));