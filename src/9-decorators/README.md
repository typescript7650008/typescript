## Decorators

Decorators é um recurso que nos permite realizar `modificações` em partes de nossos códigos no momento de sua execução.

Podemos dizer que eles são como uma anotação adicionada ao código que permite uma `modificação em seu comportamento`.

### Definição

Um decorator é basicamente uma funcão que é chamada em tempo de execução e que realiza uma modificação na estrutura onde foi anotada.

Uma Decorator pode ser aplicado a:

- Parâmetros
- Classes
- Atributos
- Métodos
- Getter e Setters

Este é recurso que já foi proposto para ser integrado de maneira nativa com o JavaScript.

Atualmente, é um recurso `experimental` no TypeScript.

Então para usar este tipo de recurso, precisamos ativar a opção `"experimentalDecorators": true,` no arquivo `tsconfig.json`.