function decoratorAttribute(
    target: unknown,
    propertyName: string,
) {
    const newName: string = `_${propertyName}`;

    Object.defineProperty(target, propertyName, {
        get() {
            return this[newName].toUpperCase();
        },
        set(newValue) {
            this[newName] = newValue.split('').reverse().join('');
        }
    })
}

class Animal {

    @decoratorAttribute
    name: string;

    constructor(name: string) {

        this.name = name;
    }
}

const cachorro = new Animal("Pluto");
console.log(cachorro.name);
