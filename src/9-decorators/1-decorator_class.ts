import { debug, log } from "./utils";

@debug
class PrimeraClasse {
    constructor() { }
}

@log
class SegundaClasse {
    constructor() { }
}

console.log(new SegundaClasse());
