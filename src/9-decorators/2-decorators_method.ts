function decoratorMethod(target: unknown, propertyKey: string, decriptor: PropertyDescriptor) {
    decriptor.value = (...args: string[]) => {
        return args.map(item => {
            return args.map(item => item.toLowerCase());
        })
    };
}

class TratarMensagem {

    @decoratorMethod
    dizerMensagem(...mensagens: string[]) {
        return mensagens;
    }

}

const intancia = new TratarMensagem();

console.log(intancia.dizerMensagem("Olá", "Seja Bem vindo", "Hcode"));