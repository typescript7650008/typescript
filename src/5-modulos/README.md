# Módulos

Um módulo é basicamente um arquivo separado que armazena uma parte de nosso código.

Ao fazer a importação de um módulo, podemos reaproveitar seu código em vários arquivos.

Ao utilizar módulos, temos acesso a alguns benefícios:

- Melhor organização do código
- Melhor reutilização do código
- Evitar conflito nos identificadores das variáveis (cada módulo tem seu próprio escopo)

### Por que são necessários?

Ao inciar um projeto em TypeScript pode ser que tenhamos poucos arquivos em um único diretório. 

Mas, conforme o projeto cresce, o número de arquivos também aumenta, o que pode deixar o nosso projeto desorganizado e confuso.

Pordemos dizer que cada arquivo TS ou JS é como uma folha de papel, ou documento.

O resultado é um código mais organizado e fácil de entender e dar manutenção.

### export deafult

Podemos usar o `export default` para exportar somente uma deternimada função, ou seja quando queremos exportar apenas um recurso, por exemplo

```javascript
    export default function convertMessage(message: string): string {
        return message.toUpperCase();
    }
```

Dessa forma podemos importar sem a necessidade do destructuring:

```javascript
import convertMessage from '../helpers/convert_message'
```

- Quando não usamos o export default, podemos exportar mais de uma função
- 