import IUser from "../interfaces/user_interface";

export default abstract class Notificaton {

    abstract send(user: IUser): boolean;
}
