import Email from "./classes/email";
import SMS from "./classes/sms";

const email = new Email();
const sms = new SMS();

email.send({
    name: "John",
    email: "johannes@gmail.com",
    phoneNumber: "35992565898",
});

sms.send({
    name: "John",
    email: "johannes@gmail.com",
    phoneNumber: "35992565898",
});