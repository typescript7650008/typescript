export interface IDatabase {
    ip: string;
    username: string;
    password: string;
    bdType: string;
}