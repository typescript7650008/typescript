import { IDatabase } from "../interfaces/database_interface";
import convertMessage from '../helpers/convert_message'

export class Database {

    static LOCAL = "127.0.0.1";
    static TYPE = "MYSQL";
    static TYPE_SQLSERVER = "SQL Server";

    constructor(
        private ip: string,
        private user: string,
        private password: string,
        private dbType: string
    ) { }

    static factory(props: IDatabase) {

        if ([Database.LOCAL, Database.TYPE_SQLSERVER].includes(props.bdType)) {
            throw new Error("Database config is not supported");
        }

        return new Database(
            props.ip,
            props.username,
            props.password,
            props.bdType,
        );

    }
}