interface IHCode {
    length: number;
}

function showTotalQuantity<T extends IHCode>(value: T): T {

    console.log(`Total ${value.length}`);

    return value;
}

console.log(showTotalQuantity([0, 1]));
console.log(showTotalQuantity("Teste"));
console.log(showTotalQuantity({ name: "Ronaldo", length: 1 }));
