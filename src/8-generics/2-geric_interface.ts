interface IProcess<T> {
    value: T;
    process(arg: T): T;
}

const texto: IProcess<string> = {
    value: "hcode",
    process(argument: string): string {
        return argument.toUpperCase();
    }

}

console.log(texto.value);
console.log(texto.process(texto.value));
console.log(texto.process("hcode treinamentos"));

const numero: IProcess<number> = {
    value: 100,
    process(value: number): number {
        return value * 2;
    }
}

console.log(numero.value);
console.log(numero.process(1500));

