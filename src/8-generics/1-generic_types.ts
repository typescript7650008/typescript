function getFirstPosition<T>(list: Array<T>): T {
    return list[0];
}

console.log(getFirstPosition(["André", "Paulo", "Marcos,"]));
console.log(getFirstPosition([50, 1, 0]));