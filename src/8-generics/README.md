## Generics

### O que são Generics

Generics nos permitem criar estruturas que serão adaptáveis a vários tipos de dados.

Esse conceito nos ajuda a reaproveitar melhor nosso código e torná-lo flexível para diversas situações.

Para definir um Generic, basta informar um tipo genérico usando os sinais de menor e maior < >.

Mesmo que definamos um tipo genérico, continuamos a ter acesso aos recursos do IntelliSense do VSCode.

Podemos cruar tipos genéricos ao trabalhar com:

- Funções
- Interface
- Classes

