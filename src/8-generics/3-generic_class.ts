interface ICadastroPadrao {
    readonly id: number;
    readonly created_at: Date;
    readonly updated_at: Date;
}

interface ICadastroUsuario extends ICadastroPadrao {
    name: string;
    email: string;
    password: string;
}

interface ICadastroCategoria extends ICadastroPadrao {
    readonly id: number;
    name: string;
}

class CadastroBasico<T> {

    create(data: T): T {
        console.log("Registro cadastrado com sucesso");
        return data;
    }

    findById(id: number): T {
        console.log(`Dados do registro: ${id}`);
        return {} as T;
    }

    update(id: number, data: T): T {
        console.log(`Editando dados do id: ${id}`);
        return {} as T;
    }

    delete(id: number): boolean {
        console.log(`Dados do registro: ${id} deletado com sucesso`);
        return true;
    }
}

class UserModel extends CadastroBasico<ICadastroUsuario>{ }

const user = new UserModel();

console.log(user.create({
    id: 1,
    name: 'Glauber',
    email: 'glauber@gmail.com',
    password: "1$12faJ87d9213",
    created_at: new Date("2022-01-01"),
    updated_at: new Date("2022-12-01"),
}));

class CategoryModel extends CadastroBasico<ICadastroCategoria>{ }

const category = new CategoryModel();

console.log(category.create({
    id: 1,
    name: "Eletrônicos",
    created_at: new Date("2022-01-01"),
    updated_at: new Date("2022-12-01"),
}));

