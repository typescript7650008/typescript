"use strict";
class People {
    constructor(name, age, height) {
        this.name = name;
        this.age = age;
        this.height = height;
    }
    toString() {
        return `${this.name}, tem ${this.age} anos de idade e ${this.height.toFixed(2)} de altura`;
    }
}
const people = new People("André", 35, 1.70);
const people2 = new People("Marcos", 39, 2);
console.log(people.name);
console.log(people2.name);
console.log(`Pessoa: ${people}`);
//# sourceMappingURL=1-first_class.js.map