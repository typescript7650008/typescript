"use strict";
class Permissao {
    constructor(_name, _nivel) {
        this._name = _name;
        this._nivel = _nivel;
    }
    get name() {
        return this._name.toUpperCase();
    }
    set name(name) {
        if (name.length < 2) {
            throw new Error("Name permissions must be at least");
        }
        this._name = name;
    }
}
const permissao = new Permissao("Administrador", 1);
console.log(permissao.name);
permissao.name = "adm";
//# sourceMappingURL=10-getter_and_setter.js.map