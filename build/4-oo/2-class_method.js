"use strict";
class Professor {
    constructor(name, age, matter) {
        this.name = name;
        this.age = age;
        this.matter = matter;
    }
    introduceYourself() {
        return `Olá meu nome é ${this.name}, tenho ${this.age} anos, e vou lecionar ${this.matter}.`;
    }
    showNotes(...notes) {
        let total = notes.reduce((total, atual) => total + atual, 0);
        return total / notes.length;
    }
}
const glaucio = new Professor("Glaucio Daniel", 45, "DBA");
console.log(glaucio.introduceYourself());
const average = glaucio.showNotes(10, 5, 8, 9);
console.log(`A media das notas é: ${average}`);
//# sourceMappingURL=2-class_method.js.map