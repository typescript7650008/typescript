"use strict";
class Usuario {
    constructor() {
        this.id = "1";
        this.name = "joao";
        this.password = "123456";
        this.created_at = new Date("2022-01-01");
    }
}
const usuario = new Usuario();
console.log(usuario.id);
/**
 * READONLY -> EU CONSIGO VER A INFORMAÇÃO
 * PRIVATE -> NÃO CONSIGO NEM VER A INFORMAÇÃO
 */ 
//# sourceMappingURL=8-class_readonly_atribute.js.map