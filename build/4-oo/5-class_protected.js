"use strict";
class Domicilio {
    constructor(cor, endereco) {
        this.cor = cor;
        this.endereco = endereco;
    }
    tocarInterfone() {
        return "Interfone tocado!";
    }
}
class Casa extends Domicilio {
    atenderInterfone(mensagem) {
        return mensagem;
    }
}
const casaDoHomer = new Casa("Rosa", { cidade: "Springfiled" });
console.log(casaDoHomer.tocarInterfone());
//# sourceMappingURL=5-class_protected.js.map