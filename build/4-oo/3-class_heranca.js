"use strict";
class Cadastro {
    constructor(name, birth_date) {
        this.name = name;
        this.birth_date = birth_date;
    }
}
class Cliente extends Cadastro {
    constructor(name, birth_date, email, company) {
        super(name, birth_date);
        this.email = email;
        this.company = company;
    }
}
const cliente = new Cliente("André", new Date("2000-01-01"), "andre@gmail.com", "XPTO");
console.log(cliente);
//# sourceMappingURL=3-class_heranca.js.map