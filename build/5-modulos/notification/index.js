"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const email_1 = __importDefault(require("./classes/email"));
const sms_1 = __importDefault(require("./classes/sms"));
const email = new email_1.default();
const sms = new sms_1.default();
email.send({
    name: "John",
    email: "johannes@gmail.com",
    phoneNumber: "35992565898",
});
sms.send({
    name: "John",
    email: "johannes@gmail.com",
    phoneNumber: "35992565898",
});
//# sourceMappingURL=index.js.map