"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Database = void 0;
class Database {
    constructor(ip, user, password, dbType) {
        this.ip = ip;
        this.user = user;
        this.password = password;
        this.dbType = dbType;
    }
    static factory(props) {
        if ([Database.LOCAL, Database.TYPE_SQLSERVER].includes(props.bdType)) {
            throw new Error("Database config is not supported");
        }
        return new Database(props.ip, props.username, props.password, props.bdType);
    }
}
exports.Database = Database;
Database.LOCAL = "127.0.0.1";
Database.TYPE = "MYSQL";
Database.TYPE_SQLSERVER = "SQL Server";
//# sourceMappingURL=Database.js.map