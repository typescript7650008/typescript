"use strict";
class CadastroBasico {
    create(data) {
        console.log("Registro cadastrado com sucesso");
        return data;
    }
    findById(id) {
        console.log(`Dados do registro: ${id}`);
        return {};
    }
    update(id, data) {
        console.log(`Editando dados do id: ${id}`);
        return {};
    }
    delete(id) {
        console.log(`Dados do registro: ${id} deletado com sucesso`);
        return true;
    }
}
class UserModel extends CadastroBasico {
}
const user = new UserModel();
console.log(user.create({
    id: 1,
    name: 'Glauber',
    email: 'glauber@gmail.com',
    password: "1$12faJ87d9213",
    created_at: new Date("2022-01-01"),
    updated_at: new Date("2022-12-01"),
}));
class CategoryModel extends CadastroBasico {
}
const category = new CategoryModel();
console.log(category.create({
    id: 1,
    name: "Eletrônicos",
    created_at: new Date("2022-01-01"),
    updated_at: new Date("2022-12-01"),
}));
//# sourceMappingURL=3-generic_class.js.map