"use strict";
const texto = {
    value: "hcode",
    process(argument) {
        return argument.toUpperCase();
    }
};
console.log(texto.value);
console.log(texto.process(texto.value));
console.log(texto.process("hcode treinamentos"));
const numero = {
    value: 100,
    process(value) {
        return value * 2;
    }
};
console.log(numero.value);
console.log(numero.process(1500));
//# sourceMappingURL=2-geric_interface.js.map