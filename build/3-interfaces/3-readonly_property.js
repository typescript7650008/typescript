"use strict";
const curso = {
    title: "TypeScript",
    price: 5000,
    hour: 10,
    classification: 5
};
// curso.price = 1000 // Erro pois tentamos alterar o valor de uma propriedade readonly
console.log(curso);
//# sourceMappingURL=3-readonly_property.js.map