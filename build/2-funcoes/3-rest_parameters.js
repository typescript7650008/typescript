"use strict";
function somaRenda(...meses) {
    return meses.reduce((total, atual) => total + atual, 0);
}
console.log(somaRenda(1000, 2000, 3000));
//# sourceMappingURL=3-rest_parameters.js.map