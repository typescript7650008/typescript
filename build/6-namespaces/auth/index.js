"use strict";
var Auth;
(function (Auth) {
    class LoginRegister {
        login(usuario) {
            return usuario;
        }
        register(novoUsuario) {
            return novoUsuario;
        }
    }
    Auth.LoginRegister = LoginRegister;
    class Recovery {
        recoveryPassword() {
            return "Enviando e-mail para recuperação de senha";
        }
        recoveryUser() {
            return "Enviando e-mail para recuperação de usuário";
        }
    }
    Auth.Recovery = Recovery;
})(Auth || (Auth = {}));
//# sourceMappingURL=index.js.map