"use strict";
// null -> ausencia de valor, nem vazio
const element = document.querySelector("h2");
let db = "mysql, 127.0.0.1, password";
db = null;
console.log("-------------------------------");
// undefined -> variavel que ainda não teve seu valor definido.
let minhaVariavel;
console.log(minhaVariavel); //undefined
if (new Date().getDate() === 15) {
    minhaVariavel = "Hoje é dia 15";
}
console.log(minhaVariavel);
//# sourceMappingURL=11_null_and_undefined.js.map