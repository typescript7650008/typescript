"use strict";
const site = "https://hcode.com.br";
let sitesFavoritos = [];
// Forma 1
sitesFavoritos.push(site); // Afirma que isso é uma string
// Forma 2
sitesFavoritos.push(site); // cast, afirma ao typescript que isso é uma string, trate-a como string.
//# sourceMappingURL=16_type_assertions.js.map