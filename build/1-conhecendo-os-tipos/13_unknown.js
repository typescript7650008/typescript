"use strict";
// unknown -> Tipo desconhecido
let resultado;
resultado = {
    sucesso: true,
};
resultado = "Deu tudo certo. OK!";
resultado = 200;
console.log("====================================");
/**
 * Diferença do unknown para any
 *
 * Quando falamos do any, ele armazena em sua variável qualquer valor e essa variavel pode ser implementada em qualquer outra variavel, porém no unknown isso não é possivel
 */
const informacao = 150;
const informacaoDesconhecida = 250;
const primeiroNumero = informacao;
//const segundoNumero: number = informacaoDesconhecida;
//# sourceMappingURL=13_unknown.js.map