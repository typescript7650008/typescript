"use strict";
// never -> tipo que nunca vai ter um valor retornado.
// Não é usado com muita constancia.
function showError(message) {
    throw new Error(message);
}
//showError("Algo deu errado");
let n = 0;
function loopInfinito() {
    while (true) {
        console.log(n++);
    }
}
loopInfinito();
//# sourceMappingURL=15_never.js.map