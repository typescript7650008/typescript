"use strict";
// void -> tipo vazio
// Serve para especificar retorno de métodos
const meuConsole = console.log("Hello TypeScript!"); // Não retorna nada!
function mostrarFase() {
    console.log("Aprendendo TypesScript");
}
mostrarFase();
//# sourceMappingURL=14_void.js.map