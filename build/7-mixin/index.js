"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const apply_mixin_1 = __importDefault(require("./apply_mixin"));
class Produto {
    vender(quantidade) {
        return `Forma vendidos ${quantidade} deste produto`;
    }
    comprar(quantidade) {
        return `Forma comprados ${quantidade} deste produto`;
    }
}
class Movel {
    sentar() {
        return "Você sentou no móvel";
    }
    empurrar(metros) {
        return `O móvel foi empurrado por ${metros} metros.}`;
    }
}
class Sofa {
    constructor(name) {
        this.name = name;
    }
}
(0, apply_mixin_1.default)(Sofa, [Produto, Movel]);
const produto = new Sofa("Meu Sofá");
produto.vender(2500);
//# sourceMappingURL=index.js.map